<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Joe
  Date: 7/21/2018
  Time: 3:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Listing Students with JDBC</title>
    <link type="text/css" rel="stylesheet" href="/css/style.css">
</head>
<body>

<div id="wrapper">
    <div id="header">
        <h2>FooBar University</h2>
    </div>
</div>

<div id="container">
    <div id="content">
        <input type="button" value="Add Student"
               onclick="window.location.href='/jdbc/add-student-form.jsp'; return false;"
               class="add-student-button"/>
        <table>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email Name</th>
                <th>Action</th>
            </tr>
            <c:forEach var="student" items="${students}">
                <c:url var="updateLink" value="/jdbc/servlets/StudentController">
                    <c:param name="command" value="GET"/>
                    <c:param name="studentId" value="${student.id}"/>
                </c:url>
                <c:url var="deleteLink" value="/jdbc/servlets/StudentController">
                    <c:param name="command" value="DELETE"/>
                    <c:param name="studentId" value="${student.id}"/>
                </c:url>
                <tr>
                    <td> ${student.firstName} </td>
                    <td> ${student.lastName} </td>
                    <td> ${student.email} </td>
                    <td><a href="${updateLink}">Update</a>
                        |
                        <a href="${deleteLink}"
                           onclick="if(!(confirm('Are you sure you to delete this student?'))) return false;">
                            Delete</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>

</body>
</html>
