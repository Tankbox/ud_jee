<%--
  Created by IntelliJ IDEA.
  User: Joe
  Date: 7/21/2018
  Time: 4:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Form to Add Student</title>
    <link type="text/css" rel="stylesheet" href="/css/style.css">
    <link type="text/css" rel="stylesheet" href="/css/add-student-style.css">
</head>
<body>
<div id="wrapper">
    <div id="header">
        <h2>FooBar University</h2>
    </div>
</div>
<div id="container">
    <h3>Add Student</h3>
    <form action="/jdbc/servlets/StudentController" method="POST">
        <table>
            <tbody>
            <tr>
                <td><label>First Name:</label></td>
                <td><input title="firstName" type="text" name="firstName"></td>
            </tr>
            <tr>
                <td><label>Last Name:</label></td>
                <td><input title="lastName" type="text" name="lastName"></td>
            </tr>
            <tr>
                <td><label>Email:</label></td>
                <td><input title="email" type="text" name="email"></td>
            </tr>
            <tr>
                <td><label></label></td>
                <td><input title="save" type="submit" value="Save" class="save"></td>
            </tr>
            </tbody>
        </table>
    </form>

    <div style="clear: both;"></div>
    <p>
        <a href="/jdbc/servlets/StudentController">Back to List</a>
    </p>
</div>

</body>
</html>
