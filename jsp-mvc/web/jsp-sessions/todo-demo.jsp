<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Joe
  Date: 7/17/2018
  Time: 10:32 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Todo List</title>
</head>
<body>

<%-- Create HTML Form--%>
<form action="todo-demo.jsp">
    Add new item: <input title="todoItem" type="text" name="todoItem">
    <input type="submit" value="Submit">
</form>

<%-- Add new item to "To Do" list --%>
<%
    @SuppressWarnings("unchecked")
    ArrayList<String> items = (ArrayList<String>) session.getAttribute("myToDoList");
    if (items == null) {
        items = new ArrayList<>();
        session.setAttribute("myToDoList", items);
    }
    String todoItem = request.getParameter("todoItem");
    if ((todoItem != null) && (!todoItem.trim().equals(""))) {
        items.add(todoItem);
    }
%>
<%-- Display all "To Do" item from the session --%>
<hr>
<b>To Do List Items:</b>
<br/>
<ol>
    <%
        for (String item : items) {
            out.println("<li>" + item + "</li>");
        }
    %>
</ol>

</body>
</html>
