<%@ page import="java.util.Date" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Joe
  Date: 7/18/2018
  Time: 8:52 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Testing JSTL</title>
</head>
<body>
<c:set var="stuff" value="<%= new Date() %>"/>
Time on the server is ${stuff}
</body>
</html>
