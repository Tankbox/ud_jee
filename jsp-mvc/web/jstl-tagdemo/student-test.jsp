<%@ page import="java.util.ArrayList" %>
<%@ page import="com.tankbox.jsp.models.Student" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Joe
  Date: 7/18/2018
  Time: 9:58 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student forEach Example</title>
</head>
<%
    ArrayList<Student> students = new ArrayList<>();
    students.add(new Student("John", "Doe", false));
    students.add(new Student("Maxwell", "Johnson", false));
    students.add(new Student("Mary", "Public", true));

    pageContext.setAttribute("students", students);
%>
<body>

<table border="1">
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Gold Customer</th>
    </tr>

    <c:forEach var="student" items="${students}">
        <tr>
            <td>${student.firstName}</td>
            <td>${student.lastName}</td>
            <td align="center">
                <c:choose>
                    <c:when test="${student.goldCustomer}">
                        Special Discount
                    </c:when>
                    <c:otherwise>
                        NO SOUP FOR YOU!
                    </c:otherwise>

                </c:choose>

            </td>
        </tr>
    </c:forEach>

</table>


</body>
</html>
