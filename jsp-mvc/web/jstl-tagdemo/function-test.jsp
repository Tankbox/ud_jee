<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%--
  Created by IntelliJ IDEA.
  User: Joe
  Date: 7/18/2018
  Time: 10:16 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Testing Function Tags</title>
</head>
<body>
<c:set var="data" value="luv2code"/>
Length of the string <b>${data}</b>: ${fn:length(data)} <br/>
Uppercase version of the string <b>${data}</b>: ${fn:toUpperCase(data)} <br/>
Does the string <b>${data}</b> start with <b>luv</b>? ${fn:startsWith(data, "luv")} <br/>
</body>
</html>
