<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Created by IntelliJ IDEA.
  User: Joe
  Date: 7/18/2018
  Time: 10:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Testing Split and Join</title>
</head>
<body>
<c:set var="data" value="Singapore,Tokyo,Mumbai,London"/>
<h3>Split Demo</h3>
<c:set var="cities" value="${fn:split(data, ',')}"/>
<c:forEach var="city" items="${cities}" >
    ${city} <br/>
</c:forEach>
<h3>Join Demo</h3>
<c:set var="cityString" value="${fn:join(cities, '*')}"/>
Result of joining: ${cityString} <br/>
</body>
</html>
