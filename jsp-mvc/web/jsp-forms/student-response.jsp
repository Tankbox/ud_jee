<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: Joe
  Date: 7/16/2018
  Time: 11:27 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student Confirmation Title</title>
</head>
<body>
The student is confirmed: ${param.firstName} ${param.lastName}
<br/>
<br/>
The student's country: ${param.country}
<br/>
<br/>
The student's favorite programming languages:
<ul>
    <%
        List<String> languages = Arrays.asList(request.getParameterValues("favoriteLanguage"));
        if (languages != null) {
            for (String language : languages) {
                out.println("<li>" + language + "</li>");
            }
        }
    %>
</ul>
</body>
</html>
