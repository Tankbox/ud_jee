<%@ page import="java.net.URLDecoder" %><%--
  Created by IntelliJ IDEA.
  User: Joe
  Date: 7/17/2018
  Time: 11:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Homepage</title>
</head>
<%
    String favLang = "Java";
    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
        for (Cookie cookie : cookies) {
            if ("myApp.favoriteLanguage".equals(cookie.getName())) {
                favLang = URLDecoder.decode(cookie.getValue(), "UTF-8");
                break;
            }
        }
    }
%>
<body>
<h4>New Books for <%= favLang %>
</h4>
<ul>
    <li>blah blah blah</li>
    <li>blah blah blah</li>
</ul>
<h4>Latest News for <%= favLang %>
</h4>
<ul>
    <li>blah blah blah</li>
    <li>blah blah blah</li>
</ul>
<h4>Hot Jobs for <%= favLang %>
</h4>
<ul>
    <li>blah blah blah</li>
    <li>blah blah blah</li>
</ul>
<a href="cookies-personalize-form.html">Personalize this page</a>
</body>
</html>
