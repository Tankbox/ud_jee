<%@ page import="javax.servlet.http.Cookie" %>
<%@ page import="java.net.URLEncoder" %>
<%--
  Created by IntelliJ IDEA.
  User: Joe
  Date: 7/17/2018
  Time: 11:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Confirmation</title>
</head>
<%
    String favLang = request.getParameter("favoriteLanguage");
    favLang = URLEncoder.encode(favLang, "UTF-8");
    Cookie cookie = new Cookie("myApp.favoriteLanguage", favLang);
    cookie.setMaxAge(60 * 60 * 24 * 365);
    response.addCookie(cookie);
%>
<body>
Thanks! We set your favorite language to: ${param.favoriteLanguage}
<br/>
<br/>
<a href="cookies-homepage.jsp">Return to homepage.</a>
</body>
</html>
