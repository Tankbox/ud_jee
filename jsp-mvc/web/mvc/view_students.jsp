<%--
  Created by IntelliJ IDEA.
  User: Joe
  Date: 7/21/2018
  Time: 1:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>MVC Example</title>
</head>
<body>

<c:forEach var="student" items="${students}" >
    ${student}<br/>
</c:forEach>

</body>
</html>
