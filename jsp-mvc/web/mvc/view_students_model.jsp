<%--
  Created by IntelliJ IDEA.
  User: Joe
  Date: 7/21/2018
  Time: 1:20 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>MVC Example with Model</title>
</head>
<body>
<h2>Student Table Demo</h2>
<hr/>
<br/>
<table border="1">
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
    </tr>
    <c:forEach var="student" items="${students}" >
        <tr>
            <td>${student.firstName}</td>
            <td>${student.lastName}</td>
            <td>${student.email}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
