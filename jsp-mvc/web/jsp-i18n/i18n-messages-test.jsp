<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%--
  Created by IntelliJ IDEA.
  User: Joe
  Date: 7/18/2018
  Time: 10:54 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Testing i18n</title>
</head>
<c:set var="locale" value="${not empty param.locale ? param.locale : pageContext.request.locale}" scope="session"/>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="i18n.labels"/>
<body>
<a href="i18n-messages-test.jsp?locale=en_US">English (US)</a>
|
<a href="i18n-messages-test.jsp?locale=es_ES">Spanish (ES)</a>
|
<a href="i18n-messages-test.jsp?locale=de_DE">German (DE)</a>
<hr/>
<fmt:message key="label.greeting"/>
<br/>
<br/>
<fmt:message key="label.firstName"/>
<i>John</i>
<br/>
<br/>
<fmt:message key="label.lastName"/>
<i>Doe</i>
<br/>
<br/>
<fmt:message key="label.weclome"/>
<hr/>
Selected locale: ${locale}
</body>
</html>
