<%--
  Created by IntelliJ IDEA.
  User: Joe
  Date: 7/16/2018
  Time: 10:46 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Declaration Test</title>
</head>
<body>
<%!
    private String makeItLower(String input) {
        return input.toLowerCase();
    }
%>

Lower case "Hello World": <%= makeItLower("Hello World.") %>
</body>
</html>
