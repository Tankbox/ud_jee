<%--
  Created by IntelliJ IDEA.
  User: Joe
  Date: 7/16/2018
  Time: 10:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Testing JSP Expressions</title>
</head>
<body>
<h3>
    JSP Expressions
</h3>
Converting a string to uppercase: <%= "Hello World!".toUpperCase() %>
<br/><br/>
25 multiplied by 4 equals <%= 25*4 %>
<br/><br/>
Is 75 less than 69? <%= 75 < 69 %>
</body>
</html>
