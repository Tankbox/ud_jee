<%--
  Created by IntelliJ IDEA.
  User: Joe
  Date: 7/16/2018
  Time: 10:58 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Testing Built in JSP Objects</title>
</head>
<body>
<h3>
    JSP Built-In Objects
</h3>
Request user agent: <%= request.getHeader("User-Agent") %>
<br/><br/>
Request language: <%= request.getLocale() %>
</body>
</html>
