package com.tankbox.jsp.servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/servlets/MvcDemoServlet")
public class MvcDemoServlet extends HttpServlet {

    public MvcDemoServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] students = {"Susan", "Anil", "Mohamed", "Trupti"};
        req .setAttribute("students", students);
        RequestDispatcher dispatcher = req.getRequestDispatcher("/mvc/view_students.jsp");
        dispatcher.forward(req, resp);
    }
}
