package com.tankbox.jsp.servlets;

import com.tankbox.jsp.models.Student;
import com.tankbox.jsp.models.StudentDataUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/servlets/MvcStudentServlet")

public class MvcStudentServlet extends HttpServlet {

    public MvcStudentServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Student> students = StudentDataUtil.getStudents();
        req.setAttribute("students", students);
        RequestDispatcher dispatcher = req.getRequestDispatcher("/mvc/view_students_model.jsp");
        dispatcher.forward(req, resp);
    }
}
