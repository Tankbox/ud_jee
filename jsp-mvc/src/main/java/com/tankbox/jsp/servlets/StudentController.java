package com.tankbox.jsp.servlets;

import com.tankbox.jsp.dao.StudentDao;
import com.tankbox.jsp.models.Student;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/jdbc/servlets/StudentController")
public class StudentController extends HttpServlet {

    private StudentDao studentDao;

    @Resource(name = "jdbc/web_student_tracker")
    private DataSource dataSource;

    @Override
    public void init() throws ServletException {
        super.init();
        studentDao = new StudentDao(dataSource);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        try {
            String command = req.getParameter("command");
            if (command == null) {
                command = "";
            }
            switch(command) {
                case "GET":
                    getStudentById(req, resp);
                    break;
                case "DELETE":
                    doDelete(req, resp);
                default:
                    listStudents(req, resp);
                    break;
            }

        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        try {
            String studentId = req.getParameter("studentId");
            if (studentId == null) {
                addStudent(req, resp);
            } else {
                updateStudent(req, resp);
            }
            listStudents(req, resp);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String studentId = req.getParameter("studentId");
            if (studentId != null) {
                deleteStudent(req, resp);
                listStudents(req, resp);
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    private void listStudents(HttpServletRequest req, HttpServletResponse resp) throws SQLException, IOException, ServletException {
        List<Student> students = studentDao.getStudents();
        req.setAttribute("students", students);
        RequestDispatcher dispatcher = req.getRequestDispatcher("/jdbc/list-students.jsp");
        dispatcher.forward(req, resp);
    }

    private void getStudentById(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        String studentId = req.getParameter("studentId");
        Student student = studentDao.getStudent(studentId);
        req.setAttribute("student", student);
        RequestDispatcher dispatcher = req.getRequestDispatcher("/jdbc/update-student-form.jsp");
        dispatcher.forward(req, resp);
    }

    private void addStudent(HttpServletRequest req, HttpServletResponse resp) throws SQLException, IOException, ServletException {
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String email = req.getParameter("email");
        Student student = new Student(firstName, lastName, email);

        studentDao.addStudent(student);
    }

    private void updateStudent(HttpServletRequest req, HttpServletResponse resp) throws SQLException, IOException, ServletException {
        String id = req.getParameter("studentId");
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String email = req.getParameter("email");
        Student student = new Student(Integer.parseInt(id), firstName, lastName, email);

        studentDao.updateStudent(student);
    }

    private void deleteStudent(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        String studentId = req.getParameter("studentId");
        studentDao.deleteStudent(studentId);
    }

}
