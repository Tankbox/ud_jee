package com.tankbox.jsp.models;

import java.util.ArrayList;
import java.util.List;

public class StudentDataUtil {

    public static List<Student> getStudents() {
        ArrayList<Student> students = new ArrayList<Student>();
        students.add(new Student("Mary", "Public", true, "mary@luv2code.com"));
        students.add(new Student("John", "Doe", false, "john@luv2code.com"));
        students.add(new Student("Ajay", "Rao", false, "ajay@luv2code.com"));
        return students;
    }
}
